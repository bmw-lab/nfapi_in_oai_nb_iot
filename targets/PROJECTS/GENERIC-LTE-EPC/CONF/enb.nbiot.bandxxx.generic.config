Active_eNBs = ( "eNB_Eurecom_LTE_NB_IoT_Box");
# Asn1_verbosity, choice in: none, info, annoying
Asn1_verbosity = "none";

eNBs =
(
 {
    ////////// Identification parameters:
    eNB_ID    =  0xe00;
    
    cell_type =  "CELL_MACRO_ENB";
    
    eNB_name  =  "eNB_Eurecom_LTE_NB_IoT_Box";
    
    // Tracking area code, 0x0000 and 0xfffe are reserved values
    tracking_area_code  =  "1";
    
    mobile_country_code =  "208";
    
    mobile_network_code =  "92";
    
       ////////// Physical parameters:
  
    component_carriers = (
    		       	 {
                           node_function                                        = "eNodeB_3GPP";
	                   	   node_timing                                          = "synch_to_ext_device";
	           	           node_synch_ref                                       = 0;
  			               frame_type					      	= "FDD";
	
			//TDD config should be not used for NB-IoT
			/*
                           tdd_config 					      	= 3;
                           tdd_config_s            			      	= 0;
			*/

			   #NORMAL cyclic prefix should be applied for DL frame
 			   prefix_type             			     	= "NORMAL";
			   #set of allowed bands TS 36.104: 1,2,3,5,8,11,12,13,17,18,19,20,25,26,28,31,66,70 
  			   eutra_band              			      	= 3;
                           downlink_frequency      			      	= 2660000000L; //arbitrary, same as enb.band7.tm1.usrpx310.conf
                           uplink_frequency_offset 			      	= -120000000; //should be the duplexing gap??
  			   Nid_cell					      	= 0;
                           N_RB_DL                 			      	= 1;//only 1 RB assigned for NB_IoT
               #it is also needed to know the NRB_DL and NRB_UL of LTE when we are in in-band and guard band mode
               #it is also needed the PRB index choosen for NB-IoT depending on the LTE bandwidht we are using (only specific indexes are allowed)
                           nb_antennas_tx          			      	= 1;
                           nb_antennas_rx          			      	= 1; 
			   tx_gain                                            	= 25;
			   rx_gain                                           	= 20;

                           nprach_CP_length              		      	= 0; // (uss66dot7 (0), us266dot7 (1))
			   #OPTIONAL (rsrp_thresholdsNPRACH)
                           rsrp_range      			      	      	= 0; // ? not in 36.331
                           nprach_Periodicity      				= 640; // ms (40,80,160,240,320,640,1280,2560)
  	                   nprach_StartTime 			      		= 8; // ms (8,16,32,64,128,256,512,1024)
                           nprach_SubcarrierOffset       			= 36; // sc (0,12,24,36,2,18,34)
			   nprach_NumSubcarriers_r13       			= 12; // (12,24,36,48)
                           nprach_SubcarrierMSG3_RangeStart           	        = "one"; // ("zero","oneThird","twoThird","one")
                           maxNumPreambleAttemptCE            			= 10; // (3,4,5,6,7,8,10) 
                           numRepetitionsPerPreambleAttempt             	= 4; // (1,2,4,8,16,32,64,128) 

                           npdcch_NumRepetitions_RA 			        = 4; // (1,2,4,8,16,32,64,128) 
                           npdcch_StartSF_CSS_RA                  	  	= 2; // (1.5,2,4,8,16,32,48,64)
                           npdcch_Offset_RA                 			= "zero"; //("zero","oneEighth","oneFourth","threeEighth")
 
                           npdsch_nrs_Power          			      	= 0; // dB (-60 .. 50)

			   ACK_NACK_NumRepetitions_NB                           = 1; //
			   threeTone_CyclicShift                                = 0; // (0,1,2)
     	                   sixTone_CyclicShift  			      	= 0; // (0 .. 3)
	                   pusch_groupAssignment      			      	= 0; // (0 .. 29)
			//OPTIONAL
			   srs_SubframeConfig					= sc0; // (0 .. 15)
	                   threeTone_BaseSequence		   	      	= 0; // (0 .. 12)
	                   sixTone_BaseSequence                                 = 0; // (0 .. 14)
	                   twelveTone_BaseSequence                              = 0; // (0 .. 30)

			//OPTIONAL
	                   dl_GapThreshold                                     	= n32; // (32,64,128,256)
	                   dl_GapPeriodicity                                    = sf64; // (64,128,256,512)
	                   dl_GapDurationCoeff                                	= "oneEighth"; // ("oneEighth","oneFourth","threeEighth","oneHalf")
			
			//ULPowerControlCommon
	                   p0_NominalNPUSCH                                     = 0; // dBm (-128 .. 24)
	                   alpha_r13                                            = al0; // (0,04,05,..1)
	                   deltaPreambleMsg3                                    = 0; // dB (-1 .. 6)
	                // msg3_delta_Preamble                                  = ; 
			   p0_UE_NPUSCH						= 0; // dB (-8 .. )

	
                           preambleTransMax_CE                          	= n3; // (3,4,5,6,7,8,10,20,50,100,200)
                           powerRampingStep                         		= dB0; // dB (0,2,4,6)
                           preambleInitialReceivedTargetPower                   = dBm-112; // dBm (-120 : 2 : -90)
                           ra_ResponseWindowSize                             	= sf20; // (20,50,80,120,180,240,320,400)
                           mac_ContentionResolutionTimer                      	= sf80; // (80,100,120,160,200,240,480,960) 
			 //OPTIONAL
                           connEstFailOffset                              	= 0; // (0 .. 15)

			   ue_TimersAndConstants_t300			      	= 1000;
			   ue_TimersAndConstants_t301			     	= 1000;
			   ue_TimersAndConstants_t310			      	= 1000;
			   ue_TimersAndConstants_t311			      	= 10000;
			   ue_TimersAndConstants_n310			      	= 20;
			   ue_TimersAndConstants_n311			      	= 1;


			///freqInfo
                  	   additionalSpectrumEmission				= 1; // (1 .. 32)
			//OPTIONAL   
			//  carrierFreq						= ;
			//  carrierFreqOffset					= ;

			///multiBandInfoList(OPTIONAL)
			   AdditionalSpectrumEmission_t				= 1; // ?

			   ue_TransmissionMode                                	= 1;
			 }
			 );



    #Default SRB1-NB configuration for RLC (same as for SIB1bis)
    srb1_NB_parameters : 
    {

	#NOTE: "N/A" means that the UE does not apply a specific value (E-UTRAN can not assume the UE to keeps the previously configured value upon switching to a default configuration

        # timer_poll_retransmit = (ms) [250, 500, 1000,..., 4000, 6000, 10000, 15000, 25000,...] TS 36.331 v14.2.0 pag 616 
        timer_poll_retransmit    = 25000;
        
	//OPTIONAL
        # timer_reordering = (ms) [0,5, ... 100, 110, 120, ... ,200]
        timer_reordering         = N/A;
        
	//OPTIONAL
        # timer_reordering = (ms) [0,5, ... 250, 300, 350, ... ,500]
        timer_status_prohibit    = N/A;
        
	//OPTIONAL
        # poll_pdu = [4, 8, 16, 32 , 64, 128, 256, infinity(>10000)]
        poll_pdu                 =  N/A;
        
	//OPTIONAL
        # poll_byte = (kB) [25,50,75,100,125,250,375,500,750,1000,1250,1500,2000,3000,infinity(>10000)]
        poll_byte                =  N/A;
        
        # max_retx_threshold = [1, 2, 3, 4 , 6, 8, 16, 32]
        max_retx_threshold       =  4;

	#enableStatusReportSN_Gap = ENUMERATED {TRUE} (Enable the status reporting due to detection of reception failure)
	enableStatusReportSN_Gap = "DISABLE"
    }
    
    # ------- SCTP definitions
    SCTP :
    {
        # Number of streams to use in input/output
        SCTP_INSTREAMS  = 2;
        SCTP_OUTSTREAMS = 2;
    };
        
    ////////// MME parameters:
    mme_ip_address      = ( { ipv4       = "192.168.13.11";
                              ipv6       = "192:168:30::17";
                              active     = "yes";
                              preference = "ipv4";
                            }
                          );

    NETWORK_INTERFACES : 
    {
        ENB_INTERFACE_NAME_FOR_S1_MME            = "eth0";
        ENB_IPV4_ADDRESS_FOR_S1_MME              = "192.168.13.10/24";

        ENB_INTERFACE_NAME_FOR_S1U               = "eth0";
        ENB_IPV4_ADDRESS_FOR_S1U                 = "192.168.13.10/24";
        ENB_PORT_FOR_S1U                         = 2152; # Spec 2152
    };
 /*
    otg_config = (
    	         {	      	
    	      	 ue_id				=1;
	     	 app_type			="scbr";
	      	 bg_traffic			="disable";
	      	 },
	      	 {	      	
    	      	 ue_id				=2;
	      	 app_type			="bcbr";
	      	 bg_traffic			="enable";
	      	 }
	      );
*/
    log_config : 
    {
	global_log_level                      ="info"; 
    	global_log_verbosity                  ="medium";
	hw_log_level                          ="debug"; 
    	hw_log_verbosity                      ="medium";
	phy_log_level                         ="info"; 
    	phy_log_verbosity                     ="medium";
	mac_log_level                         ="info"; 
    	mac_log_verbosity                     ="high";
	rlc_log_level                         ="info"; 
    	rlc_log_verbosity                     ="medium";
	pdcp_log_level                        ="info"; 
    	pdcp_log_verbosity                    ="medium";
	rrc_log_level                         ="debug"; 
    	rrc_log_verbosity                     ="medium";
	gtpu_log_level                         ="error"; 
    	gtpu_log_verbosity                     ="medium";
	udp_log_level                         ="error"; 
    	udp_log_verbosity                     ="medium";
	osa_log_level                         ="warn"; 
    	osa_log_verbosity                     ="low";

   };	
   
  }
);
